##Repositorio de Logos Personales

Esta obra de Blas Orlando Soto Muñoz está licenciada bajo la Licencia Creative Commons Atribución-SinDerivar 4.0 Internacional. Para ver una copia de esta licencia, visita http://creativecommons.org/licenses/by-nd/4.0/.

--------------

##Repository Personal Logos

This work by Blas Orlando Soto Muñoz is licensed under the Creative Commons Attribution-NoDerivatives 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
